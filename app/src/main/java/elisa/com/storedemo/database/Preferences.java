package elisa.com.storedemo.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;

public class Preferences {

    private final SharedPreferences sharedPrefereces;

    @Inject
    Preferences(Context context){
        sharedPrefereces = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public Integer getApiId(){
        return sharedPrefereces.getInt(SharedPrefs.USER_API_ID, -1);
    }

    public void insertApiId(Integer apiId) {
        sharedPrefereces.edit().putInt(SharedPrefs.USER_API_ID, apiId).apply();
    }
}
