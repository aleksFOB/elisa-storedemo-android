package elisa.com.storedemo.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public abstract class DatabasesModule {
    @Provides
    @Singleton
    static Preferences providePreferences(Context context) {
        return new Preferences(context);
    }
}
