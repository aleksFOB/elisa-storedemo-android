package elisa.com.storedemo.screens.advertisement;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import elisa.com.storedemo.di.ScreenScope;

@ScreenScope
@Subcomponent
public interface MainFragmentComponent extends AndroidInjector<MainFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainFragment> {

    }
}
