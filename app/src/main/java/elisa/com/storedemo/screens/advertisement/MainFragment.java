package elisa.com.storedemo.screens.advertisement;

import android.app.Fragment;
import android.os.Bundle;

import java.util.UUID;

import elisa.com.storedemo.R;
import elisa.com.storedemo.base.BaseFragment;

public class MainFragment extends BaseFragment {


    public static Fragment newInstance() {
        Bundle bundle = new Bundle();
        bundle.putString("instance_id", UUID.randomUUID().toString());
        Fragment fragment = new MainFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    protected int layoutRes() {
        return R.layout.fragment_main;
    }
}
