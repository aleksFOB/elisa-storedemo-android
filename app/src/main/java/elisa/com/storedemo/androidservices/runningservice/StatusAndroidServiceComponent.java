package elisa.com.storedemo.androidservices.runningservice;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import elisa.com.storedemo.di.ServiceScope;

@ServiceScope
@Subcomponent
public interface StatusAndroidServiceComponent extends AndroidInjector<StatusAndroidService> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StatusAndroidService> {
    }
}
