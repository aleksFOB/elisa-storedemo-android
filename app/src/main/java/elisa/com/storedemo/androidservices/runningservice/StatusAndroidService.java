package elisa.com.storedemo.androidservices.runningservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import javax.inject.Inject;

import elisa.com.storedemo.R;
import elisa.com.storedemo.base.BaseService;
import elisa.com.storedemo.main.MainActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static elisa.com.storedemo.base.MyApplication.CHANNEL_ID;

public class StatusAndroidService extends BaseService {

    @Inject
    StatusAndroidServiceManager serviceManager;
    @Inject
    StatusAndroidServiceState serviceState;

    public static final int JOB_ID = 0x01;

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        serviceManager.pollRepository();
        startForeground(JOB_ID, getNotification("Service running"));
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected Disposable[] subscriptions() {
        return new Disposable[]{
                serviceState.state()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                    String notificationText = getString(response);
                    Log.i("ElisaApi response", notificationText);
                    Notification notification = getNotification(notificationText);
                    updateNotification(notification);
                })
        };
    }

    private Notification getNotification(String text) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        return new NotificationCompat.Builder(this, CHANNEL_ID)
                .setPriority(Notification.PRIORITY_MIN)
                .setContentTitle("Elisa store application")
                .setContentText(text)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();
    }

    private void updateNotification(Notification notification) {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(JOB_ID, notification);
        }
    }
}
