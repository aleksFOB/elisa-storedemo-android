package elisa.com.storedemo.androidservices.runningservice;

import android.app.Service;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.ServiceKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = StatusAndroidServiceComponent.class)
public abstract class StatusAndroidServiceBindingModule {
    @Binds
    @IntoMap
    @ServiceKey(StatusAndroidService.class)
    abstract AndroidInjector.Factory<? extends Service> bindStatusAndroidService(StatusAndroidServiceComponent.Builder builder);
}
