package elisa.com.storedemo.androidservices.runningservice;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import elisa.com.storedemo.data.PollingRepository;
import elisa.com.storedemo.di.ServiceScope;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@ServiceScope
public class StatusAndroidServiceManager {
    private PollingRepository pollingRepository;
    private StatusAndroidServiceState serviceState;
    private Timer timer;

    @Inject
    StatusAndroidServiceManager(
            PollingRepository pollingRepository,
            StatusAndroidServiceState serviceState) {

        this.pollingRepository = pollingRepository;
        this.serviceState = serviceState;
    }

    @SuppressLint("CheckResult")
    public void pollRepository() {
        pollingRepository.putStatus()
//                .repeatWhen(completed ->
//                        {
//                            Log.i("Repo polling", "onComplete");
//                            return completed.delay(60, TimeUnit.SECONDS);
//                        }
//                )
//                .retryWhen(errors -> Flowable.timer(60, TimeUnit.SECONDS))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    serviceState.onSuccess();
                }, throwable -> {
                    serviceState.onError();
                });

        /*pollingRepository.putStatus()
                .doOnSubscribe(__ -> serviceState.loadingUpdated().accept(true))
                .doOnEvent((d, t) -> serviceState.loadingUpdated().accept(false))
                .subscribe(serviceState.responseUpdated(), serviceState.onError());*/
    }

    private void stopPolling() {
        timer.cancel();
    }
}
