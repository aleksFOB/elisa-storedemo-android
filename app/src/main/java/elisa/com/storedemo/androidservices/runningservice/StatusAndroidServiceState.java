package elisa.com.storedemo.androidservices.runningservice;

import android.util.Log;

import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Inject;

import elisa.com.storedemo.R;
import elisa.com.storedemo.di.ServiceScope;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

@ServiceScope
public class StatusAndroidServiceState {

    private final BehaviorRelay<Integer> stateRelay = BehaviorRelay.create();

    @Inject
    StatusAndroidServiceState() {

    }

    Observable<Integer> state() {
        return stateRelay;
    }

    Consumer<Throwable> onError() {
        Log.i("API", "Error loading response");
        stateRelay.accept(R.string.api_error);
        return throwable -> {

        };
    }

    void onSuccess() {
        Log.i("API", "Success loading response");
        stateRelay.accept(R.string.api_success);
    }
}
