package elisa.com.storedemo.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.BatteryManager;
import android.os.Build;

import static android.content.Context.BATTERY_SERVICE;

public class AndroidUtils {

    private Context context;

    AndroidUtils(Context context) {
        this.context = context;
    }

    public Integer getBatteryLevel() {
        BatteryManager bm = (BatteryManager) context.getSystemService(BATTERY_SERVICE);
        return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    }

    @SuppressLint("HardwareIds")
    public String getSerialNumber() {
        return Build.SERIAL;
    }
}
