package elisa.com.storedemo.android;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public abstract class AndroidModule {

    @Provides
    @Singleton
    static AndroidUtils provideAndroidUtils(Context context) {
        return new AndroidUtils(context);
    }
}
