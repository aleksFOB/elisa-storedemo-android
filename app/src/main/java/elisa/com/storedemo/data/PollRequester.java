package elisa.com.storedemo.data;

import javax.inject.Inject;

import io.reactivex.Completable;

public class PollRequester {

    private final PollingService pollingService;

    @Inject
    PollRequester(PollingService pollingService) {
        this.pollingService = pollingService;
    }

    retrofit2.Call<DeviceSerialResponse> getDeviceId(String serialId) {
        return pollingService.getDeviceId(serialId);
    }

    Completable putStatus(Integer id, DeviceStatusRequest deviceStatusRequest) {
        return pollingService.putDeviceStatus(id, deviceStatusRequest);
    }
}
