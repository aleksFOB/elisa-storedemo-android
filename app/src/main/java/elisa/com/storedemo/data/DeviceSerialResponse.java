package elisa.com.storedemo.data;
import com.google.auto.value.AutoValue;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

@AutoValue
public abstract class DeviceSerialResponse {
    @Json(name = "id")
    public abstract Integer id();

    public static JsonAdapter<DeviceSerialResponse> jsonAdapter(Moshi moshi) {
        return new AutoValue_DeviceSerialResponse.MoshiJsonAdapter(moshi);
    }
}
