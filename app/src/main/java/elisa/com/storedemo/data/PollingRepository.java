package elisa.com.storedemo.data;

import android.annotation.SuppressLint;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

import elisa.com.storedemo.android.AndroidUtils;
import elisa.com.storedemo.database.Preferences;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

@Singleton
public class PollingRepository {

    private Provider<PollRequester> pollRequesterProvider;
    private Scheduler scheduler;
    private Preferences preferences;
    private AndroidUtils androidUtils;

    @Inject
    PollingRepository(
            Provider<PollRequester> pollRequesterProvider,
            @Named("network_scheduler") Scheduler scheduler,
            Preferences preferences,
            AndroidUtils androidUtils) {
        this.pollRequesterProvider = pollRequesterProvider;
        this.scheduler = scheduler;
        this.preferences = preferences;
        this.androidUtils = androidUtils;
    }

    @SuppressLint("CheckResult")
    private Single<Integer> getDeviceId() {
        return Single.fromCallable(() -> {
            Integer apiId = preferences.getApiId();
            if (apiId == -1) {
                String serial = androidUtils.getSerialNumber();
                apiId = getFormattedDeviceId(serial);
                preferences.insertApiId(apiId);
            }
            return apiId;
        });
    }

    private Integer getFormattedDeviceId(String serialId) {
        Response<DeviceSerialResponse> response = null;
        try {
            response = getDeviceId(serialId).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.body().id();
    }

    private Call<DeviceSerialResponse> getDeviceId(String serialId) {
        return pollRequesterProvider.get().getDeviceId(serialId);
    }

    public Completable putStatus() {
        return getDeviceId()
                .subscribeOn(scheduler)
                .flatMapCompletable(
                        this::putStatus
                ).subscribeOn(scheduler);
    }

    private Completable putStatus(Integer id) {
        return pollRequesterProvider.get()
                .putStatus(id,  new DeviceStatusRequest(
                        androidUtils.getBatteryLevel(), "Random message"))
                .subscribeOn(scheduler);
    }
}
