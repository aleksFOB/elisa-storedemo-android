package elisa.com.storedemo.data;

import android.content.SharedPreferences;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import elisa.com.storedemo.database.SharedPrefs;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

@Module
public abstract class PollingServiceModule {
    @Provides
    @Singleton
    static PollingService providePolingService(Retrofit retrofit) {
        return retrofit.create(PollingService.class);
    }

    @Provides
    @Named("network_scheduler")
    static Scheduler provideNetworkSceduler() {
        return Schedulers.io();
    }

    @Provides
    @Named("API_id")
    static Integer provideApiId(SharedPreferences sp) {
        Integer apiId = sp.getInt(SharedPrefs.USER_API_ID, -1);
        if(apiId == -1) return null;
        return apiId;
    }
}
