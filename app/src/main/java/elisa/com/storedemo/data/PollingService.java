package elisa.com.storedemo.data;

import io.reactivex.Completable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PollingService {

    @PUT("api/devices/{id}/devicestatus")
    Completable putDeviceStatus(@Path("id") Integer id, @Body DeviceStatusRequest deviceStatusRequest);

    @GET("api/devices/serial/{serial_id}")
    Call<DeviceSerialResponse> getDeviceId(@Path("serial_id") String serialId);
}
