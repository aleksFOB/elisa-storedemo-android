package elisa.com.storedemo.data;

import com.squareup.moshi.JsonClass;

@JsonClass(generateAdapter = true)
public class DeviceStatusRequest {

    private Integer batteryLevel;

    private String message;

    public DeviceStatusRequest(Integer batteryLevel, String message) {
        this.batteryLevel = batteryLevel;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
}
