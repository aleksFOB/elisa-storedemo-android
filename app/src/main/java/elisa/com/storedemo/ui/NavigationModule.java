package elisa.com.storedemo.ui;

import dagger.Binds;
import dagger.Module;
import elisa.com.storedemo.ui.DefaultScreenNavigator;
import elisa.com.storedemo.ui.ScreenNavigator;

@Module
public abstract class NavigationModule {

    @Binds
    abstract ScreenNavigator provideScreenNavigator(DefaultScreenNavigator screenNavigator);
}
