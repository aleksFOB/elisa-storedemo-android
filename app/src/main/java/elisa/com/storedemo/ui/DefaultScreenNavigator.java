package elisa.com.storedemo.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;

import javax.inject.Inject;

import elisa.com.storedemo.R;

public class DefaultScreenNavigator implements ScreenNavigator{

    private FragmentManager fragmentManager;

    @Inject
    DefaultScreenNavigator() {
        Log.d("DefaultScreenNavigator", "Created");
    }

    @Override
    public void init(FragmentManager fragmentManager, Fragment rootFragment) {
        this.fragmentManager = fragmentManager;
        if(fragmentManager.getBackStackEntryCount() == 0) {
            fragmentManager.beginTransaction()
                    .replace(R.id.screen_container, rootFragment)
                    .commit();
        }
    }

    @Override
    public boolean pop() {
        return fragmentManager != null && fragmentManager.popBackStackImmediate();
    }

    @Override
    public void clear() {
        fragmentManager = null;
    }
}
