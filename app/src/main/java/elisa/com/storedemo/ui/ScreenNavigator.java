package elisa.com.storedemo.ui;

import android.app.Fragment;
import android.app.FragmentManager;

public interface ScreenNavigator {
    void init(FragmentManager fragmentManager, Fragment rootFragment);

    boolean pop();

    void clear();
}
