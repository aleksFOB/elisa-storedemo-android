package elisa.com.storedemo.di;

import android.app.Application;
import android.app.Service;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import dagger.android.AndroidInjector;
import elisa.com.storedemo.base.BaseService;
import elisa.com.storedemo.base.MyApplication;

public class ServiceInjector {

    private final Map<Class<? extends Service>, Provider<AndroidInjector.Factory<? extends Service>>> serviceInjectors;

    @Inject
    ServiceInjector(Map<Class<? extends Service>, Provider<AndroidInjector.Factory<? extends Service>>> serviceInjectors) {
        this.serviceInjectors = serviceInjectors;
    }

    void inject(Service service) {
        if (!(service instanceof BaseService)) {
            throw new IllegalArgumentException("Service must extend BaseService");
        }

        //noinspection unchecked
        AndroidInjector.Factory<Service> injectorFactory =
                (AndroidInjector.Factory<Service>) serviceInjectors.get(service.getClass()).get();
        AndroidInjector<Service> injector = injectorFactory.create(service);
        injector.inject(service);
    }

    public static ServiceInjector get(Application applicationContext) {
        return ((MyApplication) applicationContext).getServiceInjector();
    }
}
