package elisa.com.storedemo.di;

import android.app.Fragment;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import dagger.MapKey;

@MapKey
@Target(ElementType.METHOD)
public @interface FragmentKey {

    Class<? extends Fragment> value();
}
