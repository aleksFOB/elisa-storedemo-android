package elisa.com.storedemo.di;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.Service;

public class Injector {
    private Injector() {

    }

    public static void inject(Activity activity) {
        ActivityInjector.get(activity).inject(activity);
    }

    public static void clearComponent(Activity activity) {
        ActivityInjector.get(activity).clear(activity);
    }

    public static void inject(Fragment fragment) {
        FragmentInjector.get(fragment.getActivity()).inject(fragment);
    }

    public static void clearComponent(Fragment fragment) {
        FragmentInjector.get(fragment.getActivity()).clear(fragment);
    }

    public static void inject(Service service) {
        ServiceInjector.get((Application) service.getApplicationContext()).inject(service);
    }

    public static void clearComponent(Service service) {

    }
}
