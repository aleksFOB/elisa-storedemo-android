package elisa.com.storedemo.main;

import android.app.Fragment;

import elisa.com.storedemo.R;
import elisa.com.storedemo.base.BaseActivity;
import elisa.com.storedemo.screens.advertisement.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected Fragment initialFragment() {
        return MainFragment.newInstance();
    }
}
