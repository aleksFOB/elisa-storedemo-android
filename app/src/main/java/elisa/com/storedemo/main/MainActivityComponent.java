package elisa.com.storedemo.main;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import elisa.com.storedemo.di.ActivityScope;
import elisa.com.storedemo.androidservices.runningservice.StatusAndroidServiceBindingModule;
import elisa.com.storedemo.ui.NavigationModule;

@ActivityScope
@Subcomponent(modules = {
        MainScreenBindingModule.class,
        NavigationModule.class,
        StatusAndroidServiceBindingModule.class
})
public interface MainActivityComponent extends AndroidInjector<MainActivity>{

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {

        @Override
        public void seedInstance(MainActivity instance) {

        }
    }
}
