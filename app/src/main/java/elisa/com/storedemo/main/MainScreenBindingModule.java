package elisa.com.storedemo.main;

import android.app.Fragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import elisa.com.storedemo.di.FragmentKey;
import elisa.com.storedemo.screens.advertisement.MainFragment;
import elisa.com.storedemo.screens.advertisement.MainFragmentComponent;

@Module(subcomponents = {
        MainFragmentComponent.class
})
public abstract class MainScreenBindingModule {

    @Binds
    @IntoMap
    @FragmentKey(MainFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> bindMainFragment(MainFragmentComponent.Builder builder);
}
