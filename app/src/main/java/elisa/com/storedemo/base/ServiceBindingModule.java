package elisa.com.storedemo.base;

import android.app.Service;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.ServiceKey;
import dagger.multibindings.IntoMap;
import elisa.com.storedemo.androidservices.runningservice.StatusAndroidService;
import elisa.com.storedemo.androidservices.runningservice.StatusAndroidServiceComponent;

@Module(subcomponents = {
        StatusAndroidServiceComponent.class
})
public abstract class ServiceBindingModule {
    @Binds
    @IntoMap
    @ServiceKey(StatusAndroidService.class)
    abstract AndroidInjector.Factory<? extends Service> provideStatusAndroidServiceInjector (StatusAndroidServiceComponent.Builder builder);
}
