package elisa.com.storedemo.base;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private MyApplication myApplication;

    ApplicationModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @Provides
    Context providesApplicationContext() {
        return myApplication;
    }
}
