package elisa.com.storedemo.base;

import android.app.Service;

import elisa.com.storedemo.di.Injector;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseService extends Service {

    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    public void onCreate() {
        Injector.inject(this);
        disposables.addAll(subscriptions());
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    protected Disposable[] subscriptions() {
        return new Disposable[0];
    }
}
