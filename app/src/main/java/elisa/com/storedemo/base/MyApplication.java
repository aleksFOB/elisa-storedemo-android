package elisa.com.storedemo.base;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import javax.inject.Inject;

import elisa.com.storedemo.di.ActivityInjector;
import elisa.com.storedemo.di.ServiceInjector;

public class MyApplication extends Application {

    public static final String CHANNEL_ID = "myServiceChannel";
    @Inject
    ActivityInjector activityInjector;

    @Inject
    ServiceInjector serviceInjector;

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        applicationComponent = initComponent();
        applicationComponent.inject(this);
    }

    private ApplicationComponent initComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ActivityInjector getActivityInjector() {
        return activityInjector;
    }

    public ServiceInjector getServiceInjector() {
        return serviceInjector;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "POLLING SERVICE CHANNEL",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
