package elisa.com.storedemo.base;

import javax.inject.Singleton;

import dagger.Component;
import elisa.com.storedemo.android.AndroidModule;
import elisa.com.storedemo.database.DatabasesModule;
import elisa.com.storedemo.data.PollingServiceModule;
import elisa.com.storedemo.networking.NetworkServicesModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityBindingModule.class,
        ServiceBindingModule.class,
        NetworkServicesModule.class,
        PollingServiceModule.class,
        DatabasesModule.class,
        AndroidModule.class
})
public interface ApplicationComponent {
    void inject(MyApplication myApplication);
}
