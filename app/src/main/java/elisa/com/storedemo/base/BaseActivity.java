package elisa.com.storedemo.base;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import java.util.UUID;

import javax.inject.Inject;

import elisa.com.storedemo.di.FragmentInjector;
import elisa.com.storedemo.di.Injector;
import elisa.com.storedemo.R;
import elisa.com.storedemo.androidservices.runningservice.StatusAndroidService;
import elisa.com.storedemo.ui.ScreenNavigator;

public abstract class BaseActivity extends AppCompatActivity {
    private static final String INSTANCE_ID_KEY = "instance_id";

    private String instanceId;
    @Inject
    FragmentInjector fragmentInjector;
    @Inject
    ScreenNavigator screenNavigator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            instanceId = savedInstanceState.getString(INSTANCE_ID_KEY);
        } else {
            instanceId = UUID.randomUUID().toString();
        }
        Injector.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(layoutRes());
        ViewGroup screenContainer = findViewById(R.id.screen_container);
        if (screenContainer == null) {
            throw new NullPointerException("Activity must have a view with id: screen_container");
        }
        screenNavigator.init(getFragmentManager(), initialFragment());
        initiateApiComm();
    }

    @Override
    public void onBackPressed() {
        if (!screenNavigator.pop()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        screenNavigator.clear();
        if (isFinishing()) {
            Injector.clearComponent(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(INSTANCE_ID_KEY, instanceId);
    }

    @LayoutRes
    protected abstract int layoutRes();

    public String getInstanceId() {
        return instanceId;
    }

    protected abstract Fragment initialFragment();

    public FragmentInjector getFragmentInjector() {
        return fragmentInjector;
    }

    public void initiateApiComm() {
        Intent foregroundServiceIntent = new Intent(this, StatusAndroidService.class);
        ContextCompat.startForegroundService(this, foregroundServiceIntent);
    }
}
