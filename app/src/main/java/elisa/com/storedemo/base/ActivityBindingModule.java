package elisa.com.storedemo.base;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import elisa.com.storedemo.main.MainActivity;
import elisa.com.storedemo.main.MainActivityComponent;

@Module(subcomponents = {
        MainActivityComponent.class
})
abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> provideMainMenuActivityInjector(MainActivityComponent.Builder builder);
}
